# React Base
### Iniciamos
- Creamos una carpeta 
  > mkdir nombre_carpeta
- Nos movemos a la carpeta
  > cd nombre_carpeta
- Repositorio de forma local GIT
  > git init
- Inicializamos NPM
  > npm  init

### Instalaciones de dependencias
  - `npm i react react-dom`
  - `npm i @babel/core @babel/preset-env @babel/preset-react`
  - `npm i webpack webpack-cli webpack-dev-server`
  - `npm i babel-loader html-loader html-webpack-plugin`
  - `npm i react-router-dom`

### Agregando dependencias para trabajar estilos y navegación entre páginas
  - `npm i mini-css-extract-plugin css-loader style-loader sass sass-loader -D`
  - `npm i react-router-dom`



