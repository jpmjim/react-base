import React from 'react';
import '../styles/global.scss';

const App = () => {
  return (
    <div>
      <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/react/react-original.svg" />
      <h1 className='title'>React Base, Compartir!</h1>
    </div>
  );
}

export default App;